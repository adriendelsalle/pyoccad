.. _getting_started:

Getting started
===============

You should have `mamba <https://github.com/thesnakepit/mamba>`_ or `conda <https://github.com/conda/conda>`_ installed.

Using a Python script
---------------------

Create an environment with the minimal requirements:

.. code-block:: console

    mamba create -n pyoccad -c conda-forge pyoccad
    conda activate pyoccad

.. code-block:: console

    cat pyoccad_example.py
    > from pyoccad.create import CreatePoint
    > point = CreatePoint.as_point((0., 1., 2.))
    > print(point.Coord())

    python pyoccad_example.py

.. note::
    This example should work on both Windows and Linux. Remember to issue that command in the \
    *pyoccad* environment!

Using Jupyter
-------------

If you wish to visualize geometries, also install pyoccad-threejs which extends pyoccad with a rendering system based
on `pythreejs <https://github.com/jupyter-widgets/pythreejs>`_:

.. code-block:: console

    mamba create -n pyoccad -c conda-forge pyoccad-threejs
    conda activate pyoccad

.. note::
    Pyoccad is a dependency of pyoccad-threejs and will be installed in the same time
