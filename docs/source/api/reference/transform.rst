.. _pyoccad.transform:

pyoccad.transform
=================

.. _pyoccad.transform.BooleanOperation:

BooleanOperation
----------------
.. autoclass:: pyoccad.transform.BooleanOperation
    :members:

.. _pyoccad.transform.Filling:

Filling
-------
.. autoclass:: pyoccad.transform.Filling
    :members:

.. _pyoccad.transform.Move:

Move
----
.. autoclass:: pyoccad.transform.Move
    :members:

.. _pyoccad.transform.Rotate:

Rotate
------
.. autoclass:: pyoccad.transform.Rotate
    :members:

.. _pyoccad.transform.Scale:

Scale
-----
.. autoclass:: pyoccad.transform.Scale
    :members:

.. _pyoccad.transform.Sweep:

Sweep
-----
.. autoclass:: pyoccad.transform.Sweep
    :members:

.. _pyoccad.transform.Translate:

Translate
---------
.. autoclass:: pyoccad.transform.Translate
    :members:
